import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'

import VueResource from 'vue-resource'
Vue.use(VueResource)
// Vue.http.options.root = 'http://localhost:3000'


import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000'
axios.defaults.headers.common['Authorization'] = 'this is token'
// 所有request的header都加上Authorization
axios.defaults.headers.get['name'] = 'daniel'
// 只針對get


axios.interceptors.request.use(config => {
  console.log('request:', config)
  config['age'] = 18
  return config
})

axios.interceptors.response.use(res => {
  console.log('res:', res)
  res.config.gender = 'male'
  return res
})




Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
